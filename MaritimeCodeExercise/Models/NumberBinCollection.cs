﻿using System.Collections;
using System.Collections.Generic;

namespace MaritimeCodeExercise.Models
{
    public class NumberBinCollection : IReadOnlyList<NumberBin>
    {
        public NumberBin this[int index] => _numberBinArray[index];
        public int Count => _numberBinArray.Length;

        private readonly NumberBin[] _numberBinArray;

        public NumberBinCollection(int capacity)
        {
            _numberBinArray = new NumberBin[capacity];

            for (var i = 0; i < capacity; i++)
                _numberBinArray[i] = new NumberBin($"Bin {i + 1}");
        }

        public IEnumerator<NumberBin> GetEnumerator()
        {
            return ((IEnumerable<NumberBin>) _numberBinArray).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _numberBinArray.GetEnumerator();
        }
    }

    public class NumberBin
    {
        public string Name { get; }
        public int Frequency { get; private set; }

        public NumberBin(string name)
        {
            Name = name;
        }

        public void Increment() => Frequency += 1;
    }
}