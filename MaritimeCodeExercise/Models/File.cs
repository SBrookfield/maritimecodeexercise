﻿using System;

namespace MaritimeCodeExercise.Models
{
    public class File
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}