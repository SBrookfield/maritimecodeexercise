﻿namespace MaritimeCodeExercise.Models
{
    public class FileContent : File
    {
        public byte[] Content { get; set; }
    }
}