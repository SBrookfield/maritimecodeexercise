﻿namespace MaritimeCodeExercise.Models
{
    public class MathResult
    {
        public double? Mean { get; set; }
        public double? StandardDeviation { get; set; }
        public NumberBinCollection Frequencies { get; set; }
    }
}