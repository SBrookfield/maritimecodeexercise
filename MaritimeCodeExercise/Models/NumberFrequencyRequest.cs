﻿using System.Collections.Generic;

namespace MaritimeCodeExercise.Models
{
    public class NumberFrequencyRequest
    {
        public IEnumerable<double> Numbers { get; set; }
        public int NumberOfBins { get; set; }
        public int Interval { get; set; }

        public NumberFrequencyRequest(IEnumerable<double> numbers, int numberOfBins, int interval)
        {
            Numbers = numbers;
            NumberOfBins = numberOfBins;
            Interval = interval;
        }
    }
}