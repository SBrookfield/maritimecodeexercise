﻿using System;

namespace MaritimeCodeExercise.CustomExceptions
{
    public class DataTableUploadException : Exception
    {
        public DataTableUploadException(string message) : base(message)
        {
        }
    }
}