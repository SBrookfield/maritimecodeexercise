﻿using System;

namespace MaritimeCodeExercise.CustomExceptions
{
    public class InvalidNumberException : Exception
    {
        public string Number { get; set; }

        public InvalidNumberException(string number) : base("The number \"{}\" could not be parsed.")
        {
            Number = number;
        }
    }
}