﻿using System.Collections.Generic;
using MaritimeCodeExercise.Models;

namespace MaritimeCodeExercise.ViewModels
{
    public class FilesViewModel
    {
        public List<File> Files { get; }

        public FilesViewModel() : this(new List<File>(0))
        {
        }

        public FilesViewModel(List<File> files)
        {
            Files = files;
        }
    }
}