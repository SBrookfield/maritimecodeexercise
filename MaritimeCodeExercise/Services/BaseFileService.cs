﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using MaritimeCodeExercise.CustomExceptions;

namespace MaritimeCodeExercise.Services
{
    public interface IBaseFileService
    {
        IEnumerable<double> ParseCsvAsDoubles(byte[] bytes);
    }

    public class BaseFileService : IBaseFileService
    {
        public IEnumerable<double> ParseCsvAsDoubles(byte[] bytes)
        {
            var content = ParseBinaryContentToString(bytes);

            return content.Split(',').Select(ParseDouble);
        }

        private static double ParseDouble(string s)
        {
            if (!double.TryParse(s, out var number))
                throw new InvalidNumberException(s);

            return number;
        }

        private static string ParseBinaryContentToString(byte[] bytes)
        {
            return Encoding.Default.GetString(bytes);
        }
    }
}