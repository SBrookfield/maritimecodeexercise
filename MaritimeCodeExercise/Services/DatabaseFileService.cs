﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using MaritimeCodeExercise.DataProviders;
using MaritimeCodeExercise.Models;

namespace MaritimeCodeExercise.Services
{
    public class DatabaseFileService : BaseFileService, IFileService
    {
        private readonly IDataProvider _dataProvider;

        public DatabaseFileService(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public async Task<IEnumerable<File>> GetAllFilesAsync()
        {
            return await _dataProvider.QueryAsync<File>(StoredProcedure("dbo.[GetAllFiles]"));
        }

        public async Task<FileContent> GetFileContentsAsync(int id)
        {
            var parameters = new
            {
                id
            };

            return await _dataProvider.QuerySingleOrDefaultAsync<FileContent>(StoredProcedure("dbo.[GetFileContent]", parameters));
        }

        public async Task<File> UploadFileAsync(string fileName, byte[] content)
        {
            var parameters = new
            {
                fileName,
                content
            };

            return await _dataProvider.QuerySingleOrDefaultAsync<File>(StoredProcedure("dbo.[PersistFile]", parameters));
        }

        private static CommandDefinition StoredProcedure(string storedProcedureName, object parameters = null)
        {
            return new CommandDefinition(storedProcedureName, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}