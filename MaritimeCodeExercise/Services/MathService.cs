﻿using System;
using System.Collections.Generic;
using System.Linq;
using MaritimeCodeExercise.Models;

namespace MaritimeCodeExercise.Services
{
    public interface IAdvancedMathService
    {
        NumberBinCollection FrequenciesWithMeanAndList(NumberFrequencyRequest numberFrequencyRequest, out double mean, out List<double> list);
        double StandardDeviationWithMean(IEnumerable<double> numbers, double mean);
    }

    public interface IMathService
    {
        NumberBinCollection Frequencies(NumberFrequencyRequest request);
        double StandardDeviation(IEnumerable<double> numbers);
        double SquareRoot(double number);
        double Square(double number);
        double Mean(IEnumerable<double> numbers);
    }

    public class MathService : IMathService, IAdvancedMathService
    {
        public NumberBinCollection Frequencies(NumberFrequencyRequest request)
        {
            // Take the memory hit for code maintainability.
            // I don't want to create the list, but this code becomes too complex and duplicating it to remove the list means I'd have to manage two implementations.
            return FrequenciesInternal(request, out _, out _);
        }

        public NumberBinCollection FrequenciesWithMeanAndList(NumberFrequencyRequest request, out double mean, out List<double> list)
        {
            var frequencies = FrequenciesInternal(request, out list, out var sum);
            mean = sum / list.Count;
            return frequencies;
        }

        private static NumberBinCollection FrequenciesInternal(NumberFrequencyRequest request, out List<double> numbers, out double sum)
        {
            numbers = new List<double>();
            sum = 0d;

            var maxNumber = 0d;
            var mustCalculateNumberOfBins = request.NumberOfBins == 0;

            var binCollection = mustCalculateNumberOfBins
                ? null
                : new NumberBinCollection(request.NumberOfBins);

            foreach (var number in request.Numbers)
            {
                numbers.Add(number);
                sum += number;
                
                if (number > maxNumber)
                    maxNumber = number;

                if (mustCalculateNumberOfBins)
                    continue;

                // If we're not auto-calculating the number of bins, go ahead and "place the number in the bin".
                var index = GetBinIndexForNumber(request.Interval, request.NumberOfBins, number);
                binCollection[index].Increment();
            }

            if (!mustCalculateNumberOfBins)
                return binCollection;
            
            // Cast to int to truncate the max number's decimal point, then add 1 to simulate calculating the ceiling.
            var numberOfBins = (int) (maxNumber / request.Interval) + 1;
            binCollection = new NumberBinCollection(numberOfBins);

            foreach (var index in numbers.Select(number => GetBinIndexForNumber(request.Interval, numberOfBins, number)))
                binCollection[index].Increment();

            return binCollection;
        }

        public double StandardDeviation(IEnumerable<double> numbers)
        {
            var collection = numbers as ICollection<double> ?? numbers.ToList();
            return StandardDeviationInternal(collection, Mean(collection));
        }

        public double StandardDeviationWithMean(IEnumerable<double> numbers, double mean)
        {
            return StandardDeviationInternal(numbers, mean);
        }

        private double StandardDeviationInternal(IEnumerable<double> numbers, double mean)
        {
            var squaredDifferences = numbers.Select(number => Square(number - mean));
            var squaredDifferencesMean = Mean(squaredDifferences);
            return SquareRoot(squaredDifferencesMean);
        }

        public double SquareRoot(double number)
        {
            return SquareRoot(number, 10);
        }

        public double Square(double number)
        {
            return number * number;
        }

        public double Mean(IEnumerable<double> numbers)
        {
            return Sum(numbers, out var count) / count;
        }

        public double Sum(IEnumerable<double> numbers)
        {
            return Sum(numbers, out _);
        }

        private static int GetBinIndexForNumber(int interval, int numberOfBins, double number)
        {
            for (var i = 0; i < numberOfBins; i++)
                if (number < (i + 1) * interval)
                    return i;

            // If there's no match, place it in the last bin.
            return numberOfBins - 1;
        }

        private static double SquareRoot(double number, double initialSquareRoot )
        {
            while (true)
            {
                var squareRoot = (initialSquareRoot + number / initialSquareRoot) / 2;

                if (Math.Abs(squareRoot - initialSquareRoot) < 0.01)
                    return squareRoot;

                initialSquareRoot = squareRoot;
            }
        }

        private static double Sum(IEnumerable<double> numbers, out int count)
        {
            count = 0;
            var sum = 0d;

            foreach (var number in numbers)
            {
                sum += number;
                count++;
            }

            return sum;
        }
    }
}