﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MaritimeCodeExercise.Models;

namespace MaritimeCodeExercise.Services
{
    public interface IFileService : IBaseFileService
    {
        Task<IEnumerable<File>> GetAllFilesAsync();
        Task<FileContent> GetFileContentsAsync(int id);
        Task<File> UploadFileAsync(string fileName, byte[] content);
    }
}