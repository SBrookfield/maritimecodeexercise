﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;

namespace MaritimeCodeExercise.DataProviders
{
    public interface IDataProvider
    {
        Task<IEnumerable<T>> QueryAsync<T>(CommandDefinition commandDefinition);
        Task<T> QuerySingleOrDefaultAsync<T>(CommandDefinition commandDefinition);
        Task ExecuteAsync(CommandDefinition commandDefinition);
    }
}