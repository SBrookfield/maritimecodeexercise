﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace MaritimeCodeExercise.DataProviders
{
    public class SqlDataProvider : IDataProvider
    {
        private readonly string _connectionString;

        public SqlDataProvider(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("MaritimeCodeExerciseDatabase");
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(CommandDefinition commandDefinition)
        {
            using var connection = await GetConnection();
            return await connection.QueryAsync<T>(commandDefinition);
        }

        public async Task<T> QuerySingleOrDefaultAsync<T>(CommandDefinition commandDefinition)
        {
            using var connection = await GetConnection();
            return await connection.QuerySingleOrDefaultAsync<T>(commandDefinition);
        }

        public async Task ExecuteAsync(CommandDefinition commandDefinition)
        {
            using var connection = await GetConnection();
            await connection.ExecuteAsync(commandDefinition);
        }

        private async Task<IDbConnection> GetConnection()
        {
            var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            return connection;
        }
    }
}