﻿// Change the file select to match the file name.
$(".custom-file-input").on("change", function () {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

var createAlert = function(message) {
    var alertElement = $(
            "<div id=\"fileAlert\"class=\"alert alert-danger alert-dismissible fade show mt-2\" role=\"alert\">" +
                message +
                "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
                "<span aria-hidden=\"true\">&times;</span>" +
                "</button>" +
            "</div>"
        );

    setTimeout(function () { alertElement.alert("close"); }, 3000);

    return alertElement;
};

var getMessageForStatus = function(status) {
    switch (status) {
        case 422:
            return "Oops! There isn't anything to upload!";
        case 415:
            return "Tut tut. I validate on the server too!";
        case 413:
            return "Eek! Your file was too large!";
        default:
            return "Oh no! An error occured whilst uploading your file.";
    }
};

$("form").submit(function(event) {
    event.preventDefault();

    var file = $("#fileUpload").prop("files");

    if (!file)
        return;

    var formData = new FormData();
    formData.append("formFile", file[0]);

    $.ajax({
        url: "/api/files",
        type: "POST",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    }).done(function (file) {
        addTableRow(file);
        $("#fileUpload").val("");
        $("label[for='fileUpload']").text("Choose file...");
    })
        .fail(function (xhr) {
            $("form").append(createAlert(getMessageForStatus(xhr.status)));
    });
});

var tableRowClicked = function (event) {
    var primaryClass = "table-primary";
    $("tbody").children().removeClass(primaryClass);
    $(event.target).parent().addClass(primaryClass);

    $("button").prop("disabled", false);
    $("button").prop("disabled", false);
    $("input").prop("disabled", false);

    clearData();
};

var addTableRow = function(file) {
    var dateCreated = new Date(file.dateCreated).toLocaleString();
    var tableRow = $(
        "<tr>" +
        "<th scope=\"row\" >" + file.id + "</th>" +
        "<td>" + file.fileName + "</td>" +
        "<td>" + dateCreated + "</td>" +
        "</tr >"
    );

    tableRow.click(tableRowClicked);

    $("table > tbody:last").append(tableRow);
};

var getSelectedFileId = function () {
    return $("tbody .table-primary").children(":first").text();
};

var createBin = function(name, number) {
    return $(
        "<div class=\"card m-1\">" +
            "<div class=\"card-header font-weight-bold\">" + name + "</div>" +
            "<ul class=\"list-group list-group-flush\">" +
                "<li class=\"list-group-item\">" + number + " items</li>" +
            "</ul>" +
        "</div>"
    );
}

var clearData = function() {
    $("#meanContainer").text("");
    $("#standardDeviationContainer").text("");
    $("#frequenciesContainer").empty();
};

var setData = function(data) {
    if (!data)
        return;

    if (data.mean)
        $("#meanContainer").text(data.mean);

    if (data.standardDeviation)
        $("#standardDeviationContainer").text(data.standardDeviation);

    if (data.frequencies) {
        var container = $("#frequenciesContainer").empty();
        data.frequencies.forEach(function (bin) {
            container.append(createBin(bin.name, bin.frequency));
        });
    }
};

var runCalculation = function(method, queryString) {
    var fileId = getSelectedFileId();
    var url = "/api/Math/" + method + "/" + fileId;

    if (queryString)
        url += "?" + queryString;

    $.get(url, setData)
        .fail(function() {
            $("#results").prepend(createAlert("Oops! Something went wrong with this calculation."));
        });
};

$("#calculateMean").click(function () {
    runCalculation("Mean");
});

$("#calculateStandardDeviation").click(function () {
    runCalculation("StandardDeviation");
});

$("#calculateEverything").click(function () {
    runCalculation("All");
});

var getQueryString = function() {
    var numberOfBins = $("#specifyNumberOfBins").is(':checked') ? $("#numberOfBins").val() : 0;
    return "numberOfBins=" + numberOfBins + "&interval=" + $("#interval").val();
};

var setIntervalText = function () {
    var interval = $("#interval").val();
    var text = "[(0,<" + interval + "), (" + interval + ",<" + interval * 2 + "), ... ,]";
    $("#intervalText").text(text);
};

$(document).ready(function() {
    var buttons = [
        {
            text: "Calculate Mean",
            click: function() { runCalculation("Mean"); }
        },
        {
            text: "Calculate Standard Deviation",
            click: function () { runCalculation("StandardDeviation"); }
        },
        {
            text: "Calculate Frequencies",
            click: function () { runCalculation("Frequencies", getQueryString()); }
        },
        {
            text: "Calculate Everything",
            click: function () { runCalculation("All", getQueryString()); }
        }
    ];
    var buttonsContainer = $("#calculateButtons");

    buttons.forEach(
        function(button) {
            buttonsContainer.append(
                $("<button/>")
                    .prop("disabled", true)
                    .addClass("btn btn-primary m-2")
                    .text(button.text)
                    .click(button.click)
            );
        }
    );

    $("table > tbody > tr").click(tableRowClicked);
    $("#numberOfBinsValue").text($("#numberOfBins").val());
    $("#intervalValue").text($("#interval").val());
    setIntervalText();
});

$("#specifyNumberOfBins").change(function () {
    if (this.checked) {
        $("#numberOfBins").parent().show();
    } else {
        $("#numberOfBins").parent().hide();
    }
});

$("#numberOfBins").on("input", function() {
    $("#numberOfBinsValue").text($("#numberOfBins").val());
});

$("#interval").on("input", setIntervalText);