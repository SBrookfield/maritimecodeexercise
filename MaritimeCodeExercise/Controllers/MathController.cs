﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using MaritimeCodeExercise.Models;
using MaritimeCodeExercise.Services;
using Microsoft.AspNetCore.Mvc;

namespace MaritimeCodeExercise.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MathController : ControllerBase
    {
        private readonly IFileService _fileService;
        private readonly IMathService _mathService;
        private readonly IAdvancedMathService _advancedMathService;

        public MathController(IFileService fileService, IMathService mathService, IAdvancedMathService advancedMathService)
        {
            _fileService = fileService;
            _mathService = mathService;
            _advancedMathService = advancedMathService;
        }

        [HttpGet("Mean/{fileId}")]
        public async Task<ActionResult> Mean(int fileId)
        {
            return await GetMathResult(fileId, result => result.Mean, ms => ms.Mean);
        }

        [HttpGet("StandardDeviation/{fileId}")]
        public async Task<ActionResult> StandardDeviation(int fileId)
        {
            return await GetMathResult(fileId, result => result.StandardDeviation, ms => ms.StandardDeviation);
        }

        [HttpGet("Frequencies/{fileId}")]
        public async Task<ActionResult> Frequencies(int fileId, [FromQuery] int numberOfBins = 10, [FromQuery] int interval = 10)
        {
            return await GetMathResult(fileId, (result, numbers) =>
                {
                    result.Frequencies = _mathService.Frequencies(new NumberFrequencyRequest(numbers, numberOfBins, interval));
                });
        }

        [HttpGet("All/{fileId}")]
        public async Task<ActionResult> All(int fileId, [FromQuery] int numberOfBins = 10, [FromQuery] int interval = 10)
        {
            return await GetMathResult(fileId, (mathResult, numbers) =>
            {
                // Using a special call, we can reduce the iterations since both the frequency and mean iterate the collection.
                mathResult.Frequencies = _advancedMathService.FrequenciesWithMeanAndList(new NumberFrequencyRequest(numbers, numberOfBins, interval), out var mean, out var numbersAsList);
                mathResult.Mean = mean;

                // Avoid re-calculating the mean (removes a loop, and removes all the addition) by passing the already calculated mean.
                mathResult.StandardDeviation = _advancedMathService.StandardDeviationWithMean(numbersAsList, mean);

            });
        }
        
        private async Task<ActionResult> GetMathResult(int fileId, Expression<Func<MathResult, double?>> propertyFunc, Func<IMathService, Func<IEnumerable<double>, double>> mathServiceFunc)
        {
            return await GetMathResult(fileId, (mathResult, numbers) =>
            {
                if (propertyFunc.Body is MemberExpression memberExpression && memberExpression.Member is PropertyInfo propertyInfo)
                    propertyInfo.SetValue(mathResult, mathServiceFunc(_mathService)(numbers));
            });
        }

        private async Task<ActionResult> GetMathResult(int fileId, Action<MathResult, IEnumerable<double>> resultAction)
        {
            var numbers = await GetNumbersFromFileById(fileId);

            if (numbers == null)
                return NotFound();

            var mathResult = new MathResult();

            try
            {
                resultAction(mathResult, numbers);
            }
            catch(Exception exception)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, new
                {
                    message = exception.Message
                });
            }

            return Ok(mathResult);
        }

        private async Task<IEnumerable<double>> GetNumbersFromFileById(int fileId)
        {
            var file = await _fileService.GetFileContentsAsync(fileId);

            return file == null 
                ? null 
                : _fileService.ParseCsvAsDoubles(file.Content);
        }
    }
}