﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using MaritimeCodeExercise.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MaritimeCodeExercise.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IFileService _fileService;

        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await _fileService.GetAllFilesAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var fileContent = await _fileService.GetFileContentsAsync(id);

            if (fileContent == null)
                return NotFound();

            return Ok(fileContent);
        }

        [HttpPost]
        public async Task<ActionResult> Post(IFormFile formFile)
        {
            if (formFile == null)
                return StatusCode((int) HttpStatusCode.UnprocessableEntity);

            if (!string.Equals(Path.GetExtension(formFile.FileName), ".csv", StringComparison.OrdinalIgnoreCase))
                return StatusCode((int) HttpStatusCode.UnsupportedMediaType);

            var fileLengthInMegabytes = formFile.Length / 1024 / 1024;
            if (fileLengthInMegabytes > 2)
                return StatusCode((int) HttpStatusCode.RequestEntityTooLarge);

            var content = new byte[formFile.Length];

            await using (var stream = formFile.OpenReadStream())
                await stream.ReadAsync(content, 0, content.Length);

            return Ok(await _fileService.UploadFileAsync(formFile.FileName, content));
        }
    }
}
