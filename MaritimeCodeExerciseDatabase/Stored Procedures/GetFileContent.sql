﻿CREATE PROCEDURE [dbo].[GetFileContent]
	@id int
AS
BEGIN
	SELECT [Id], [FileName], [Content], [DateCreated], [DateUpdated]
	FROM Files
	WHERE Id = @id
END