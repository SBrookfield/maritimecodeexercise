﻿CREATE PROCEDURE [dbo].[PersistFile]
	@fileName varchar(255),
	@content varbinary(max)
AS
BEGIN
	INSERT Files ([FileName], [Content], [DateCreated])
	VALUES (@fileName, @content, GETDATE())
	
	SELECT [Id], [FileName], [Content], [DateCreated]
	FROM Files
	WHERE Id = SCOPE_IDENTITY()
END