﻿CREATE PROCEDURE [dbo].[GetAllFiles]
AS
BEGIN
	SELECT [Id], [FileName], [DateCreated], [DateUpdated]
	FROM Files
END